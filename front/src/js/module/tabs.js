new Vue({
    el: '#app',
    data: {
        message: 'Frequently asked questions',
        tabs: {
            'General': {
                items: [
                    {
                        title: 'General Title 1',
                        body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nam alias architecto officia, dolores animi qui debitis incidunt eius temporibus nostrum nihil soluta commodi molestiae necessitatibus ducimus amet. Suscipit, saepe!'
                    },
                    {
                        title: 'General Title 2',
                        body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nam alias architecto officia, dolores animi qui debitis incidunt eius temporibus nostrum nihil soluta commodi molestiae necessitatibus ducimus amet. Suscipit, saepe!'
                    },
                    {
                        title: 'General Title 3',
                        body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nam alias architecto officia, dolores animi qui debitis incidunt eius temporibus nostrum nihil soluta commodi molestiae necessitatibus ducimus amet. Suscipit, saepe!'
                    },
                    {
                        title: 'General Title 4',
                        body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nam alias architecto officia, dolores animi qui debitis incidunt eius temporibus nostrum nihil soluta commodi molestiae necessitatibus ducimus amet. Suscipit, saepe!'
                    },
                ]

            },
            'Creators': {
                items: [
                    {
                        title: 'Creators Title 1',
                        body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nam alias architecto officia, dolores animi qui debitis incidunt eius temporibus nostrum nihil soluta commodi molestiae necessitatibus ducimus amet. Suscipit, saepe!'
                    },
                ]
            },
            'Bites': {
                items: [
                    {
                        title: 'Bites Title 1',
                        body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nam alias architecto officia, dolores animi qui debitis incidunt eius temporibus nostrum nihil soluta commodi molestiae necessitatibus ducimus amet. Suscipit, saepe!'
                    },
                    {
                        title: 'Bites Title 2',
                        body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nam alias architecto officia, dolores animi qui debitis incidunt eius temporibus nostrum nihil soluta commodi molestiae necessitatibus ducimus amet. Suscipit, saepe!'
                    },
                    {
                        title: 'Bites Title 3',
                        body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nam alias architecto officia, dolores animi qui debitis incidunt eius temporibus nostrum nihil soluta commodi molestiae necessitatibus ducimus amet. Suscipit, saepe!'
                    },
                    {
                        title: 'Bites Title 4',
                        body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nam alias architecto officia, dolores animi qui debitis incidunt eius temporibus nostrum nihil soluta commodi molestiae necessitatibus ducimus amet. Suscipit, saepe!'
                    },
                ]
            },
            'Groups': {
                items: [
                    {
                        title: 'Groups Title 1',
                        body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nam alias architecto officia, dolores animi qui debitis incidunt eius temporibus nostrum nihil soluta commodi molestiae necessitatibus ducimus amet. Suscipit, saepe!'
                    },
                ]
            },
            'Last': {
                items: [
                    {
                        title: 'Last Title 1',
                        body: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab nam alias architecto officia, dolores animi qui debitis incidunt eius temporibus nostrum nihil soluta commodi molestiae necessitatibus ducimus amet. Suscipit, saepe!'
                    },
                ]
            },
        },
        activeTab: 'General',
    },
    computed:{
        tabContent() {
            return this.tabs[this.activeTab];
        },
    },
    methods: {
        setTabActive(tab) {
            this.activeTab = tab;
        }
    },
})