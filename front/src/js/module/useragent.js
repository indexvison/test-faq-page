let deviceAgent = navigator.userAgent.toLowerCase();
const body = document.querySelector("body");
if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
    body.classList.add("ios");
    body.classList.add("mobile");
}
if (navigator.userAgent.search("MSIE") >= 0) {
    body.classList.add("ie");
} else if (navigator.userAgent.search("Chrome") >= 0) {
    body.classList.add("chrome");
} else if (navigator.userAgent.search("Firefox") >= 0) {
    body.classList.add("firefox");
} else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
    body.classList.add("safari");
} else if (navigator.userAgent.search("Opera") >= 0) {
    body.classList.add("opera");
}